﻿using System;
using System.Runtime.Serialization;

namespace Ejercicio_4
{
    class Electrodomestico
    {
        private float precio_base = 100;
        private string color = "blanco";
        private char consumo_energetico = 'F';
        private float peso = 5;

        public Electrodomestico() { }

        public Electrodomestico(float peso, float precio)
        {
            this.precio_base = precio;
            this.peso = peso;
        }
        public Electrodomestico(float precio, string color, float peso, char consumo_energetico)
        {
            this.precio_base = precio;
            this.peso = peso;
            comprobarConsumoEnergetico(consumo_energetico);
            comprobarColor(color);
        }
        public float Precio_base
        {
            get
            {
                return this.precio_base;
            }
        }
        public string Color
        {
            get
            {
                return this.color;
            }
        }
        public char Consumo_energetico
        {
            get
            {
                return this.consumo_energetico;
            }
        }
        public float Peso
        {
            get
            {
                return this.peso;
            }
        }
        private void comprobarConsumoEnergetico(char v)
        {
            const string letras = "ABCDEF";
            if (letras.Contains(v))
            {
                this.consumo_energetico = v;
            }
            else
            {
                this.consumo_energetico = 'F';
            }
        }
        private void comprobarColor(string c)
        {
            const string colores = "BLANCONEGROROJOAZULGRIS";
            if (colores.Contains(c.ToUpper()))
            {
                this.color = c.ToUpper();
            }
            else
            {
                this.color = "BLANCO";
            }
        }
        public float precioFinal()
        {
            float precio = precio_base;
            switch (this.consumo_energetico)
            {
                case 'A':
                    precio_base += 100;
                    break;
                case 'B':
                    precio_base += 80;
                    break;
                case 'C':
                    precio_base += 60;
                    break;
                case 'D':
                    precio_base += 50;
                    break;
                case 'E':
                    precio_base += 30;
                    break;
                case 'F':
                    precio_base += 10;
                    break;
            }
            if (this.peso > 0 && this.peso < 19)
            {
                this.peso += 10;
            }
            else if (this.peso > 20 && this.peso < 49)
            {
                this.peso += 50;
            }
            else if (this.peso > 50 && this.peso < 79)
            {
                this.peso += 80;
            }
            else if (this.peso > 80)
            {
                this.peso += 100;
            }
            return precio;
        }
    }
    class Lavadora : Electrodomestico
    {

        private int carga = 5;
        public Lavadora() { }

        public Lavadora(float precio, float peso) : base(peso, precio) { }

        public Lavadora(float precio, float peso, string color, char consumo_energetico, int carga) : base(precio, color, peso, consumo_energetico)
        {
            this.carga = carga;
        }
        public int Carga
        {
            get
            {
                return this.carga;
            }
        }
        public float precioFinal()
        {
            float precio = base.precioFinal();
            if (carga > 30)
            {
                precio += 50;
            }
            return precio;
        }
    }
    class Television : Electrodomestico
    {
        private int resolucion = 20;
        private bool sintonizador = false;

        public Television() : base() { }
        public Television(float precio, float peso) : base(precio, peso) { }

        public Television(float precio, float peso, string color, char consumo_energetico, int resolucion, bool sintonizador) : base(precio, color, peso, consumo_energetico)
        {
            this.resolucion = resolucion;
            this.sintonizador = sintonizador;
        }

        public int Resolucion
        {
            get
            {
                return this.resolucion;
            }
        }
        public bool Sintonizador
        {
            get
            {
                return this.sintonizador;
            }
        }
        public float precioFinal()
        {
            float precio = base.precioFinal();

            if (this.sintonizador)
            {
                precio += 50;
            }
            if (this.resolucion > 40)
            {
                precio *= 1.300F;
            }
            return precio;
        }

    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Electrodomestico[] electrodomesticos = new Electrodomestico[10];
            electrodomesticos[0] = new Television(10, 15);
            electrodomesticos[1] = new Lavadora(13, 25);
            electrodomesticos[2] = new Lavadora(18, 30);
            electrodomesticos[3] = new Television(22, 43);
            electrodomesticos[4] = new Television(40, 57);
            electrodomesticos[5] = new Lavadora(45, 64);
            electrodomesticos[6] = new Television(50, 75);
            electrodomesticos[7] = new Lavadora(54, 78);
            electrodomesticos[8] = new Television(61, 81);
            electrodomesticos[9] = new Lavadora(67, 85);

            int televisor = 0, lavarropas = 0;
            foreach(Electrodomestico e in electrodomesticos)
            {
                e.precioFinal();
                if (e is Television)
                {
                    televisor++;
                }
                if(e is Electrodomestico)
                {
                    lavarropas++;
                }
            }
            Console.WriteLine("Televisores: {0} \n Lavarropas {1} \n Electrodomesticos {2}", televisor, lavarropas, televisor + lavarropas);

        }
    }
}

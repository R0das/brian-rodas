﻿using System;

namespace Ejercicio_1
{
    class Cuenta
    {

        private string titular;
        private double cantidad;

        Cuenta(string titular, double cant)
        {
            this.cantidad = cant;
            this.titular = titular;
        }
        Cuenta(string titular)
        {
            this.titular = titular;
            this.cantidad = 0;
        }
        public string Titular
        {
            get
            {
                return this.titular;
            }
            set
            {
                this.titular = value;
            }
        }
        public double Cantidad
        {
            get
            {
                return this.cantidad;
            }
            set
            {
                this.cantidad = value;
            }
        }
        public void ingresar(double val)
        {
           
            if(val > 0)
            {
                this.cantidad += val;
            }
            else
            {

            }
        }
        public void retirar(double val)
        {
            this.cantidad -= val;
            if(this.cantidad < 0)
            {
                this.cantidad = 0;
            }
        }
    }
}

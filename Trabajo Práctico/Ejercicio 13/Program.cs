﻿using System;
using System.Collections.Generic;

namespace Ejercicio_13
{
    class Producto
    {
        protected string nombre;
        protected float precio;
        public Producto(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public float getPrecio
        {
            get
            {
                return this.precio;
            }
        }
        public float setPrecio
        {
            set
            {
                this.precio = value;
            }
        }

        public string toString()
        {
            return String.Format("El producto {0} tiene un valor de {1}", this.nombre, this.precio);
        }
        public void calcular(int productos)
        {
            this.precio *= productos;
        }
    }
    class Perecedero : Producto
    {
        private int dias_a_caducar;
        public Perecedero(string nombre, float precio, int dias_a_caducar) : base(nombre, precio)
        {
            this.dias_a_caducar = dias_a_caducar;
        }
        public int getDiasACaducar
        {
            get
            {
                return this.dias_a_caducar;
            }
        }
        public int setDiasACaducar
        {
            set
            {
                this.dias_a_caducar = value;
            }
        }
        public new void calcular(int productos)
        {
            base.calcular(productos);
            switch (this.dias_a_caducar)
            {
                case 1:
                    this.precio /= 4;
                    break;
                case 2:
                    this.precio /= 3;
                    break;
                case 3:
                    this.precio /= 2;
                    break;
                default:
                    break;
            }
        }
        public new string toString()
        {
            return String.Format("{0} es perecedero y caduca en {1} dias", base.toString(), this.dias_a_caducar);
        }
    }
    class NoPerecedero : Producto
    {
        private string tipo;
        public NoPerecedero(string nombre, float precio, string tipo) : base(nombre, precio)
        {
            this.tipo = tipo;
        }
        public string getTipo
        {
            get
            {
                return this.tipo;
            }
        }
        public string setTipo
        {
            set
            {
                this.tipo = value;
            }
        }
        public new string toString()
        {
            return String.Format("{0} es no-perecedero y de tipo {1}", base.toString(), this.tipo);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Producto> productos = new List<Producto>();
            productos.Add(new Perecedero("Leche", 10, 5));
            productos.Add(new Perecedero("Queso", 20, 2));
            productos.Add(new Perecedero("Yogurt", 15, 1));
            productos.Add(new NoPerecedero("Arroz", 10, "Legumbre"));
            productos.Add(new NoPerecedero("Fideos", 17, "Pasta"));
            foreach (Producto p in productos)
            {
                if (p is Perecedero)
                {
                    Perecedero pp = (Perecedero)p;
                    pp.calcular(5);
                    Console.WriteLine(pp.toString());
                }
                else if (p is NoPerecedero)
                {
                    NoPerecedero np = (NoPerecedero)p;
                    np.calcular(5);
                    Console.WriteLine(np.toString());
                }
            }
        }
    }
}

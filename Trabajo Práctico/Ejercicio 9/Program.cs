﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejercicio_9
{
    class Cine
    {
        private List<List<Asiento>> asientos = new List<List<Asiento>>();
        private Pelicula pelicula;
        private float precio_entrada;

        public Cine(int filas, int columnas, float precio, Pelicula pelicula)
        {
            this.precio_entrada = precio;
            this.pelicula = pelicula;
            int columnas_t = (columnas > 26) ? 26 : columnas;
            for (int i = 0; i < filas; i++)
            {
                List<Asiento> fila_temp = new List<Asiento>();
                for (int z = 0; z < columnas_t; z++)
                {
                    fila_temp.Add(new Asiento((char)(z + 65), i + 1));
                }
                this.asientos.Add(fila_temp);
            }
        }
        public List<List<Asiento>> getAsientos
        {
            get
            {
                return this.asientos;
            }
        }
        public float getPrecio
        {
            get
            {
                return this.precio_entrada;
            }
        }
        public Pelicula getPelicula
        {
            get
            {
                return this.pelicula;
            }
        }
        public List<List<Asiento>> setAsientos
        {
            set
            {
                this.asientos = value;
            }
        }
        public float setPrecio
        {
            set
            {
                this.precio_entrada = value;
            }
        }
        public Pelicula setPelicula
        {
            set
            {
                this.pelicula = value;
            }
        }
        public bool hayLugar()
        {
            foreach (List<Asiento> fila in asientos)
            {
                foreach (Asiento a in fila)
                {
                    if (a.getEspectador == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool hayLugarButaca(int fila, char col)
        {
            int col_index = letra_col_to_index(col);
            if (col_index == -1)
            {
                return false;
            }
            if (fila >= asientos.Count)
            {
                return false;
            }
            return (asientos[fila][col_index].getEspectador == null);
        }
        public List<Asiento> getButacasDisponibles()
        {
            List<Asiento> temp = new List<Asiento>();
            foreach (List<Asiento> fila in this.asientos)
            {
                foreach (Asiento a in fila)
                {
                    if (!a.ocupado())
                    {
                        temp.Add(a);
                    }
                }
            }
            return temp;
        }
        public bool sePuedeSentar(Espectador e)
        {
            return (e.tieneEdad(this.pelicula.getEdadMinima) && e.tieneDinero(this.precio_entrada));
        }
        public void sentar(int fila, char col, Espectador e)
        {
            this.asientos[fila][letra_col_to_index(col)].setEspectador = e;
        }
        public Asiento getAsiento(int fila, char col)
        {
            return this.asientos[fila][this.letra_col_to_index(col)];
        }
        private int letra_col_to_index(char col)
        {
            int col_index = -1;
            if (((int)col >= 65 && (int)col <= 90) || (int)col >= 97 && (int)col <= 122)
            {
                if ((int)col <= 90)
                {
                    col_index = (int)col - 65;
                }
                else
                {
                    col_index = (int)col - 97;
                }
            }
            return col_index;
        }
        public int getFilas()
        {
            return asientos.Count;
        }

        public string toString()
        {
            string s = String.Format("Se esta dando la pelicula: {0} \n Hay {1} filas y {2} columnas. \n Los asientos son: \n", this.pelicula.toString(), this.getFilas(), this.asientos[0].Count);
            foreach (List<Asiento> l in this.asientos)
            {
                foreach (Asiento a in l)
                {
                    s += a.toString();
                }
            }
            return s;
        }
    }

    class Pelicula
    {
        private string titulo;
        private int duracion;
        private int edad_minima;
        private string director;
        public Pelicula(string titulo, int duracion, int edad_minima, string director)
        {
            this.titulo = titulo;
            this.director = director;
            this.edad_minima = edad_minima;
            this.duracion = duracion;
        }
        public string setTitulo
        {
            set
            {
                this.titulo = value;
            }
        }
        public int setDuracion
        {
            set
            {
                this.duracion = value;
            }
        }
        public int setEdadMinima
        {
            set
            {
                this.edad_minima = value;
            }
        }
        public string setDirector
        {
            set
            {
                this.director = value;
            }
        }
        public string getTitulo
        {
            get
            {
                return this.titulo;
            }
        }
        public int getDuracion
        {
            get
            {
                return this.duracion;
            }
        }
        public string getDirector
        {
            get
            {
                return this.director;
            }
        }
        public int getEdadMinima
        {
            get
            {
                return this.edad_minima;
            }
        }
        public string toString()
        {
            return string.Format("{0} del director {1}, con una duracion de {2} minutos y la edad minima es de {3} años", this.titulo, this.director, this.duracion, this.edad_minima);
        }
    }
    class Espectador
    {
        private string nombre;
        private int edad;
        private float dinero;
        public Espectador(string nombre, int edad, float dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public int getEdad
        {
            get
            {
                return this.edad;
            }
        }
        public float getDinero
        {
            get
            {
                return this.dinero;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public int setEdad
        {
            set
            {
                this.edad = value;
            }
        }
        public float setDinero
        {
            set
            {
                this.dinero = value;
            }
        }
        public bool tieneEdad(int edad)
        {
            return (this.edad >= edad);
        }
        public bool tieneDinero(float dinero)
        {
            return (this.dinero >= dinero);
        }
    }
    class Asiento
    {
        private char letra;
        private int fila;
        private Espectador espectador = null;

        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
        }
        public char getLetra
        {
            get
            {
                return this.letra;
            }
        }
        public int getFila
        {
            get
            {
                return this.fila;
            }
        }
        public Espectador getEspectador
        {
            get
            {
                return this.espectador;
            }
        }
        public char setLetra
        {
            set
            {
                this.letra = value;
            }
        }
        public int setFila
        {
            set
            {
                this.fila = value;
            }
        }
        public Espectador setEspectador
        {
            set
            {
                this.espectador = value;
            }
        }
        public bool ocupado()
        {
            return (this.espectador != null);
        }
        public string toString()
        {
            if (this.ocupado())
            {
                return String.Format("Asiento {0} {1} y {2}", this.fila, this, letra, this.espectador);
            }
            return String.Format("Asiento {0} {1} se encuentra vacio.", this.fila, this.letra);
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Pelicula pelicula = new Pelicula("Dragon Ball Z: la resurrección de Freezer", 94, 7, "Akira Toriyama");
            Console.WriteLine("Ingrese un numero para la cantidad de filas:");
            int filas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese un numero entre el 1 y el 26 para la cantidad de columnas:");
            int cols = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese un numero para el precio de la entrada:");
            int precio = int.Parse(Console.ReadLine());
            Cine cine = new Cine(filas, cols, precio, pelicula);
            Console.WriteLine("Ingrese un numero para la cantidad de espectadores:");
            int c_espectadores = int.Parse(Console.ReadLine());
            List<Espectador> espectadores = new List<Espectador>();
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            for (int i = 0; i < c_espectadores; i++)
            {
                espectadores.Add(new Espectador(i.ToString(), rdn.Next(5, 35), rdn.Next(0, 1000)));
                if (cine.hayLugar())
                {
                    if (cine.sePuedeSentar(espectadores.Last()))
                    {
                        List<Asiento> l = cine.getButacasDisponibles();
                        Asiento a = l[rdn.Next(0, l.Count)];
                        cine.sentar(a.getFila, a.getLetra, espectadores.Last());
                    }
                }
            }
            Console.WriteLine(cine.toString());
        }

    }
}

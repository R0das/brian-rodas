﻿using System;
using System.Collections.Generic;

namespace ej12
{
    class Revolver
    {
        private int pos_actual;
        private int pos_bala;

        public Revolver()
        {
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            pos_actual = rdn.Next(0, 5);
            pos_bala = rdn.Next(0, 5);
        }
        public bool dispar()
        {
            if (this.pos_actual == this.pos_bala)
            {
                this.siguienteBala();
                return true;
            }
            this.siguienteBala();
            return false;
        }
        public void siguienteBala()
        {
            this.pos_actual = (pos_actual + 1) % 6;
        }
        public string toString()
        {
            return String.Format("La posicion actual del tambor es: {0} y la posicion de la bala es: {1}", this.pos_actual, this.pos_bala);
        }
    }
    class Jugador
    {
        private int id;
        private string nombre;
        private bool vivo = true;
        public Jugador(int id)
        {
            this.id = id;
            this.nombre = String.Format("Jugador {0}", id);
        }
        public bool getVivo
        {
            get
            {
                return this.vivo;
            }
        }
        public bool disparar(Revolver r)
        {
            this.vivo = !r.dispar();
            return !this.vivo;
        }
    }
    class Juego
    {
        List<Jugador> jugadores;
        Revolver revolver = new Revolver();
        public Juego(int c_jugadores)
        {
            int j = c_jugadores % 7;
            for (int z = 0; z < j; z++)
            {
                jugadores.Add(new Jugador(z + 1));
            }
        }
        public bool finJuego()
        {
            foreach (Jugador j in this.jugadores)
            {
                if (!j.getVivo)
                {
                    return true;
                }
            }
            return false;
        }

        public void ronda()
        {
            foreach (Jugador j in this.jugadores)
            {
                if (j.disparar(revolver))
                {
                    break;
                }
            }
        }

    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese un numero entre el 1 y el 6 para la cantidad de jugadores");
            int cant_j = int.Parse(Console.ReadLine());
            Juego juego = new Juego(cant_j);
            while (!juego.finJuego())
            {
                juego.ronda();
            }
        }
    }
}


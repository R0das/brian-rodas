﻿using System;
using System.Reflection.PortableExecutable;

namespace Ejercicio_2
{
    class Persona
    {
        System.Random random = new System.Random();
        private string nombre = "";
        private int edad = 0;
        private int DNI;
        private char sexo = 'H';
        private float peso = 0;
        private float altura = 0;

        public Persona() 
        {
            this.generarDNI();
        }

        public Persona(string nombre, int edad, char sexo)
        {
            this.generarDNI();
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;

        }
        public Persona(string nombre, int edad, int DNI, char sexo, float peso, float altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.DNI = DNI;
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;

        }
        public int calcularIMC()
        {
            float pesoIdeal = this.peso / (float)Math.Pow(this.altura, 2);
            if(pesoIdeal < 20)
            {
                return -1;
            }else if(pesoIdeal >= 20 && pesoIdeal <= 25)
            {
                return 0;
            }else
            {
                return 1;
            }
        } 
        public bool esMayorDeEdad()
        {
            if(this.edad > 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void comprobarSexo()
        {
            if (!(this.sexo == 'H' || this.sexo == 'M'))
            {
                this.sexo = 'H';
            }
        }
        private void generarDNI()
        {
            this.DNI = random.Next(10000000, 99999999);
        }
        public string Nombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            set
            {
                this.edad = value;
            }
        }
        public char Sexo
        {
            set
            {
                this.sexo = value;
            }
        }
        public float Peso
        {
            set
            {
                this.peso = value;
            }
        }
        public float Altura
        {
            set
            {
                this.altura = value;
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese el nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese la edad");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el sexo (H para Hombre o M para Mujer)");
            char sexo = Console.ReadLine()[0];
            Console.WriteLine("Ingrese el nombre");
            float peso = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la altura");
            float altura = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el dni");
            int dni = int.Parse(Console.ReadLine());


            Persona[] objeto = new Persona[3];

            objeto[0] = new Persona(nombre, edad, dni, sexo, peso, altura);
            objeto[1] = new Persona(nombre, edad, sexo);
            objeto[2] = new Persona();
            objeto[2].Nombre = "Juan";
            objeto[2].Edad = 26;
            objeto[2].Altura = 183.5F; 
            objeto[2].Peso = 83.3F;
            objeto[2].Sexo = 'H';

            foreach(Persona p in objeto)
            {
                switch (p.calcularIMC())
                {
                    case -1:
                        Console.WriteLine("Se encuentra por debajo de su peso ideal");
                        break;
                    case 0:
                        Console.WriteLine("Tiene su peso ideal");
                        break;
                    case 1:
                        Console.WriteLine("Tiene sobrepeso");
                        break;
                }
            }
        }
    }

}

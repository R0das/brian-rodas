﻿using System;

namespace Ejercicio_11
{
class Jugador
{
    private string nombre;
    private float dinero;
    private int porras_ganadas;
    private string[] resultados;

    public Jugador(string nombre)
    {
        this.nombre = nombre;
        this.dinero = 50;
        this.resultados = new string[10];
        this.reiniciarResultados();
    }
    public bool puedePagar(float costo)
    {
        return this.dinero >= costo;
    }
    public void ponerEnLaVaquita(float costo)
    {
        this.dinero -= costo;
        Console.WriteLine("El jugador: {0} sumo {1} pesos a la vaquita y le quedan {2} pesos", this.nombre, costo, this.dinero);
    }
    public void reiniciarResultados()
    {
        for (int z = 0; z < 10; z++)
        {
            this.resultados[z] = "";
        }
    }
    public void generarResultados()
    {
        Random rdn = new Random(DateTime.UtcNow.Millisecond);
        for (int w = 0; w < resultados.Length; w++)
        {
            int local = rdn.Next(0, 10);
            int visitante = rdn.Next(0, 10);
            resultados[w] = String.Format("{0} - {1}", local, visitante);
            Console.WriteLine("El jugador {0} eligio el resultado {1}", this.nombre, resultados[w]);
        }
    }
    public bool haAcertado(string[] lista)
    {
        for (int q = 0; q < this.resultados.Length; q++)
        {
            if (this.resultados[q] != lista[q])
            {
                return false;
            }
        }
        return true;
    }
    public void ganarVaquita(float dinero)
    {
        this.porras_ganadas++;
        this.dinero += dinero;
        Console.WriteLine("El jugador {0} ha ganado {1} pesos y tiene {2} pesos", this.nombre, dinero, this.dinero);
    }
    public string toString()
    {
        return String.Format("El jugador {0} tiene {1} pesos y ha ganado {2} porras", this.nombre, this.dinero, this.porras_ganadas);
    }
}
class Porra
{
    private float vaquita = 0;
    Jugador[] jugadores;
    public Porra(Jugador[] jugadores)
    {
        this.jugadores = jugadores;
    }
    public void aumentarVaquita(float dinero)
    {
        this.vaquita += dinero;
    }
    public void vaciarVaquita()
    {
        this.vaquita = 0;
    }
    public void jornadas()
    {
        Resultados resultados = new Resultados();
        string[] partidos;
        for (int r = 0; r < 10; r++)
        {
            Console.WriteLine("Jornada {0}", r + 1);
            for (int w = 0; w < jugadores.Length; w++)
            {
                if (jugadores[w].puedePagar(10))
                {
                    jugadores[w].ponerEnLaVaquita(10);
                    jugadores[w].generarResultados();
                    this.aumentarVaquita(10);
                }
                else
                {
                    jugadores[w].reiniciarResultados();
                }
            }
        }
        resultados.generarResultados();
        partidos = resultados.getPartidos;
        for (int h = 0; h < jugadores.Length; h++)
        {
            if (jugadores[h].haAcertado(partidos))
            {
                jugadores[h].ganarVaquita(this.vaquita);
                this.vaciarVaquita();
            }
        }
        for (int x = 0; x < jugadores.Length; x++)
        {
            Console.WriteLine(jugadores[x].toString());
        }
    }
}
class Resultados
{
    private string[] partidos;

    public Resultados()
    {
        this.partidos = new string[10];
    }
    public void generarResultados()
    {
        Random rdn = new Random(DateTime.UtcNow.Millisecond);
        for (int w = 0; w < partidos.Length; w++)
        {
            int local = rdn.Next(0, 10);
            int visitante = rdn.Next(0, 10);
            partidos[w] = String.Format("{0} - {1}", local, visitante);
            Console.WriteLine("El partido {0} ha generado el resultado {1}", (w + 1), partidos[w]);
        }
    }
    public string[] getPartidos
    {
        get
        {
            return this.partidos;
        }
    }
}
class Ejecutable
{
    static void Main(string[] args)
    {
        Jugador[] jugadores = new Jugador[7];
        jugadores[0] = new Jugador("Juan");
        jugadores[1] = new Jugador("Jose");
        jugadores[2] = new Jugador("Raul");
        jugadores[3] = new Jugador("Agustin");
        jugadores[4] = new Jugador("Tomas");
        jugadores[5] = new Jugador("Damian");
        jugadores[6] = new Jugador("Pablo");
        Porra p = new Porra(jugadores);
        p.jornadas();
    }
}
}


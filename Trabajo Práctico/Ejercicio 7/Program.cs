﻿using System;

namespace Ejercicio_7
{
    class Raices
    {
        private double a;
        private double b;
        private double c;

        public Raices(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;

        }

        public void obtenerRaices()
        {
            

            double resultado_pos = ((this.b * -1) + Math.Sqrt((Math.Pow(this.b, 2)) - ((4 * this.a) * this.c))) / (2 * this.a);
            double resultado_neg = ((this.b * -1) - Math.Sqrt((Math.Pow(this.b, 2)) - ((4 * this.a) * this.c))) / (2 * this.a);

        }
    }
}
